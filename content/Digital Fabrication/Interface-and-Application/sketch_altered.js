//For the #WCCChallenge, theme: "squishy"
let r;
let layers = [];
let bg = 255;
let tbg = 0;
let fcol;
let lean = 0;
let lvel = 0;
let stretch = 0;
let svel = -5;

function setup() {
	cnv = createCanvas(600, 600);
    cnv.parent('altered-canvas-container');
	r = height / 3;
	for (let y = height / 3; y > -height / 3; y -= height /36) {
		let h = map(y, height / 3, -height / 3, 3, 4);
		let verts = [];
		for (let a = 0; a <= TAU; a += TAU / 30) {
			verts.push(new Vert(a, r, createVector(0, 0),y));
		}
		layers.push(new Layer(verts, h));
	}
	fcol = color(random(255), random(255), random(255), 100);
}

function draw() {
	lean += lvel;
	lvel += (mouseX - pmouseX) / (100 * height);
	lvel -= lean / 20;
	lvel *= 0.85;
	stretch += svel;
	svel += (mouseY - pmouseY) / height;
	svel -= stretch / 20;
	svel *= 0.9;
	bg = lerp(bg, tbg, 0.1);
	background(bg);
	fill(fcol);
	stroke(255, 100);
	strokeWeight(height / 220);
	translate(width / 2, height / 2);
	for (let l of layers) {
		l.jiggle();
	}
}

function mousePressed() {
	tbg = 255 - tbg;
	if (tbg == 0) fcol = color(random(255), random(255), random(255), 100);
}

class Layer {
	constructor(verts, h) {
		this.verts = verts;
		this.h = h;
	}
	jiggle() {
		rotate(lean);
		translate(0, stretch);
		beginShape();
		for (let v of this.verts) {
			v.r += v.rv;
			v.rv -= (v.r - r) / 10;
			// if (movedX!=0 || movedY!=0 ) {
			// let d = dist(v.pos.x, v.pos.y, mouseX - width / 2, mouseY - height / 2) / 2;
			// v.rv -= r / max((height / 50), d);
			// }
			v.rv *= 0.9;
			v.pos.x = v.r * cos(v.a);
			v.pos.y = v.basey + (v.r / this.h) * sin(v.a);
			curveVertex(v.pos.x, v.pos.y);
		}
		endShape(CLOSE);
	}
}

class Vert {
	constructor(a, r, pos, basey) {
		this.a = a;
		this.r = r;
		this.rv = 0;
		this.pos = pos;
		this.basey = basey;
	}
}