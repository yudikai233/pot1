---
title: Final Project
description: "Description page for the final project of Digital Fabrication"
bookFlatSection: false
bookCollapseSection: true
weight: 1
---
# Final project

This page serves as a progress tracker for the ideation and creation of the final project in Digital Fabrication. For more information, you can check:

- [Weekly updates](/digital-fabrication/final-project/weekly-progress/) - weekly diaries of the project development process.
- [References](/digital-fabrication/final-project/references/) - compilation of reference videos, artworks, and articles that have inspired and informed the project's development.
- [Abandoned Ideas](/digital-fabrication/final-project/abandoned-ideas/) - other ideas that I have envisionlized for the final project.

---
