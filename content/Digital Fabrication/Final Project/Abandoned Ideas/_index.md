---
title: Abandoned Ideas
weight: 100
---
This page is used to present the ideas that I 
# 1 'Libra_mini' reconstruction

## What is *Libra_mini*
![libra_mini](libra62.jpg)

I have always been keen on mechanical keyboards, and among the many keyboards that I used, a particular one stands out, which is a 40% keyboard called ***Libra_mini***. 

A friend of mine designed this marvel, managing to implement the Alice layout with a special tented design in such a compact form factor. And to compensate for the trade-off of getting rid of the arrow keys, he decide to put an Xbox joystick on the bottom right, bringing even more unexcepted applications that both increased productivity and also improved ergonomics.

There are some videos about the structure of the keyboard and the build process as follows.

{{<youtube XwKQwry9zPc>}}
<br>
{{<youtube ugGHH7QWEys>}}



### What I am gonna do with it
For the final project of the Digital Fabrication I course, I would like to take the current *Libra_mini* design to another level, and hopefully reconstruct it since I did not bring the one I have to Finland. The original one uses acrylic as the main material, but this time I would like to try other materials such as wood or 3D printing. If this task is too hard, I would still have some other opinions such as switching to another keyboard such as *Spring* which has a simpler structure, or doing some installation art pieces.