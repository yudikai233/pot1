---
title: Computer-Aided Design
weight: 20
---

# Computer-aided Design

## Intro

In this page, I will present my learning path for trying out different CAD software programs, and how I use those software programs to create a parametric design for the Computer-cutting project. 

This page will be separated into three sections:

- [Introduction & comparison](#introduction--comparison)
- [Modeling process (and more)](#Modeling)
- [User experience & final conclusion](#final-conclusion)

---

## Introduction

There are three software programs that came to my mind when I saw this week's task: FreeCAD, Blender, and Fusion 360. Each of them are have unique features that distinct them with their competitors, however they also have their own disadvantages, so it is crucial to try them by yourself and see which one suits you best.

According to **New Bing**:

- FreeCAD 

FreeCAD is an open source CAD software that is free to use and extend. It allows you to keep control of your files, which are stored locally on your device. It has a modular architecture that supports various workflows and disciplines. However, it has a steeper learning curve than Fusion 360 and Blender, and it is still under development, so it may have some bugs or limitations. 

*(Added by me) Some other advantages: being available on every platform, easy to install, and relatively small file size.*

- Blender 

Blender is also an open source software that is mainly used for 3D modeling, animation, rendering, and game development. It has a large and active community that provides tutorials, add-ons, and support. It can also handle some CAD tasks such as boolean operations, mesh editing, sculpting, etc. However, it is not designed specifically for CAD purposes, so it may lack some features or precision that other CAD software have.

*(Added by me) There are some add-ons that are mainly targeted on the CAD modeling task for blender, some of them is said to be quite good but I haven't tried them.*

- Fusion 360

Fusion 360 is a cloud-based CAD/CAM/CAE software that offers a comprehensive set of tools for design, simulation, manufacturing, collaboration, and more. It has a user-friendly interface that is easy to learn and use. It also has various extensions and plug-ins that enhance its functionality. However, it is not free for commercial use or hobbyists (unless they qualify for certain criteria), and it requires an internet connection to access your files.

*(Added by me) The forced internet connection is both a good thing and a bad thing. On the bright side it helped me to sync design between different devices as well as provided version control, and would probably be more useful is multiple people are contributing to the same design. However, this feature is not that good when you are editing your design on the go and couldn't find a proper internet connection.*

## Comparison
Based on these information(which are mostly accurate after searching myself), I made a comparison table that listed the pros and cons of these software programs.

|                | FreeCAD            | Blender                   | Fusion 360                   |
|----------------|--------------------|---------------------------|------------------------------|
| Price          | Free (Open source) | Free (Open Source)        | $545/year (Personal use)     |
| User-friendly  | Not so good        | Quite good                | Good                         |
| Learning-curve | Steep              | Shallow                   | In between                   |
| Features       | Normal             | Many, but few are for CAD | Many                         |
| Shortcomming   | UI                 | Not meant to do CAD       | Forced online                |

Despite being more difficult to learn, I chose to try out FreeCAD first as it's open source nature would probably be beneficial in the future (when we can not get a educational license for Fusion 360). For this week's project, I only used FreeCAD for modeling. However in the following projects, I decided to switch to Fusion 360 for the modeling work. There are multiple reasons for me to make this switch:

- Most of our classmates and former students uses Fusion 360, which make it much easier for troubleshooting since I can learn from their experiences.
- The user interface especially the tool bar in FreeCAD is more difficult to navigate compare with Fusion 360.
- Great systematic tutorial for Fusion 360: [All New Fusion 360 for 3D Printing for Absolute Beginners.](https://www.youtube.com/playlist?list=PLGs0VKk2DiYwxUjGRWEgotTY8ipVvFsIp)

As for Blender, I use it more for rendering rather than modeling, but it's new geometry node feature is very interesting and I tried modeling with that in the later 3D printing week.

---

## Designing the model

For the ideation of the design, I checked [Lining Yao's project](http://fab.cba.mit.edu/classes/863.11/people/lining.yao/design%20Projects/project1.html), where she implemented several design based on the work of [Yoshinobu Miyamoto](https://www.flickr.com/photos/yoshinobu_miyamoto/sets/). 

{{< img e1.jpg "img_center shadow" >}}

I found Miyamoto's origami design with geometric shapes very interesting, and decided to try making another star-shaped one in the following picture with different material such as cardboard or plywood. 

{{< img e4.jpg "img_center wide shadow" >}}

### Modeling basic shape



These designs are mostly centrosymmetric with 9 identical triangle-shaped parts, thus means only one part would have to be made to create the whole design. Therefore, we can focus on the parameters of this one shape and generate the others.

{{< img 1.jpg "img_center shadow" >}}

Initially I worked with the spreadsheet with the basic parameters, which are the height, width, and thickness of the triangle.

{{< img 2.jpg "img_center shadow" >}}

Then add constrains in sketch mode to implement the parameter to the triangle.

{{< img 3.jpg "img_center shadow" >}}

Then pad the triangle with the thickness of the material. 

{{< img 3.5.jpg "img_center shadow" >}}

A few more parameter would have to be added which are the tilting angle of the part, the number of the parts, and the distance between the parts and the central point. Using these parameters we can control the position of each parts in the design, and how they are assembled together.

{{< img 4.jpg "img_center shadow" >}}

So in order to create the cuts used for assembling, we would have to add the neighboring 4 parts to simulate the actual scenario
create the cuts so that they can be assembled together.

{{< img 5.jpg "img_center shadow" >}}

I used the `Boolean Cut Tool` from the `Parts` menu to create the final part which have the full cut.

---

## Blender

{{< img render3.jpg "img_center shadow" >}}

After that, I used blender to have a overview for the final form. I set up some basic lighting and made some render. <br>
The shape is not exactly identical to the original origami design since we have to count the thickness of the material in, but I think it looks good enough. It would be better if I can make it in the computer-controlled cutting section next week.

{{< img render4.jpg "img_center shadow" >}}