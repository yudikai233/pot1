---
title: Input Device
weight: 110
---

# Input Device

## Intro

In this page, I will document the process of using the MPU-6050 6-DoF Accel and Gyro sensor, and connect it to Touchdesigner to get the data visualized.

---

## Basic info

{{<img sensor.jpg "half img_center shadow" "MPU6050 Module">}}

{{<img MPU6050-Pinout.png "half img_center shadow" "Pinout">}}

Specs:
- Main Chip: MPU-6050
- Power supply: 3~5V
- Communication mode: standard IIC communication protocol
- Chip built-in 16bit AD converter, 16bit data output
- Gyroscopes range: +/- 250 500 1000 2000 degree/sec
- Acceleration range: ±2 ±4 ±8 ±16g 

Connection
| MPU-6050 Pin | Micro-controller Pin |
|-----|-------------------------------|
| VCC | 5v                            |
| GND | GND                           |
| SCL | D5 SCL                        |
| SDA | D4 SDA                        |


## Hardware

I designed and fabricated a new board for adding this sensor. Unfortunately the board broke in half when I was adding rivets.


{{<img pcb_broken.jpg "wide img_center shadow" "💔">}}

After another board milling session and carefully adding rivets, I finally get the board done. Next I will definitely use the SMD pin sockets as they do not need such process, even it's a little bit harder to solder.

{{<img pcb_final.jpg "wide img_center shadow" "Pins on the left are for OLED">}}

## Library

I tried different libraries for the sensor, including the `Adafruit_MPU6050 library` and `MPU6050_light`. I chose the latter one because it has a detailed documentation which goes through all the calculation process. Also it has the zeroing function for yaw angle calculation. 

- [MPU6050_light Github repository](https://github.com/rfetick/MPU6050_light)
- [MPU6050_light Documentation](https://github.com/rfetick/MPU6050_light/blob/master/documentation_MPU6050_light.pdf)

{{<img MPU6050_light1.jpg "wide img_center shadow">}}

## Arduino programming

I changed a little to get a serial output that was easier to read for the touchdesigner.

```
/* Get tilt angles on X and Y, and rotation angle on Z
 * Angles are given in degrees
 * 
 * License: MIT
 */

#include "Wire.h"
#include <MPU6050_light.h>

MPU6050 mpu(Wire);
unsigned long timer = 0;

void setup() {
  //Disabled all the unnecessary serial output due to the

  Serial.begin(9600);
  Wire.begin();
  
  byte status = mpu.begin();
  //Serial.print(F("MPU6050 status: "));
  //Serial.println(status);
  while(status!=0){ } // stop everything if could not connect to MPU6050
  
  //Serial.println(F("Calculating offsets, do not move MPU6050"));
  delay(1000);
  // mpu.upsideDownMounting = true; // uncomment this line if the MPU6050 is mounted upside-down
  mpu.calcOffsets(); // gyro and accelero
  //Serial.println("Done!\n");
}

void loop() {
  mpu.update();
  
  if((millis()-timer)>10){ // print data every 10ms

  Serial.print(mpu.getAngleX());Serial.print(",");
  Serial.print(mpu.getAngleY());Serial.print(",");
  Serial.print(mpu.getAngleZ());Serial.println();

  // Old outputs
	// Serial.print("X : ");
	// Serial.print(mpu.getAngleX());
	// Serial.print("\tY : ");
	// Serial.print(mpu.getAngleY());
	// Serial.print("\tZ : ");
	// Serial.println(mpu.getAngleZ());
	timer = millis();  
  }
}

```

## Touchdesigner

{{<img TD1.jpg "full img_center shadow" "Reading inputs for serial">}}

{{<img TD2.jpg "full img_center shadow" "Visualizing data">}}

## Results

{{< video result.mp4 "img_center shadow wide">}}

Changes on all three directions (Pitch, yaw and roll) are detected. Pitch and yaw angles are precise, roll angle have a slight offset which resulted in an increasing error as time goes on. A good zeroing process can limit this error to less than 1° in a few minutes, which I think is well enough for my use case.

Shout out to the guy who write the `MPU-6050_light` library. This made using the sensor so much easier and more accurate. I'm happy for the results that I have so far.

For further use on this sensor, you can refer to the [Shape of time II](/embodied-interaction/final-project) project that I did for the [Embodied Interaciton](/embodied-interaction) course.