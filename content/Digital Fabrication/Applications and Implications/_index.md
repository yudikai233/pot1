---
title: Applications and Implications
weight: 170
---

# Applications and Implications

## Intro

In this page I will be discussing the applications and implications of my final project. Presumably, this will include a summary and reevaluation of the previous design work, as well as creating a BOM (Bill of Materials) for the project. 

## Summary of the Project

As the selection of the final project topic have been changed in the middle of the course, there are two different projects that I have been working on. 

The first idea is to optimize a keyboard design, and utilizing the resources available in Fablab to manufacture it. Although this I didn't continue with this idea as the final project, it served as an inspiration for exploring other possibilities related to keyboards, which later transformed into the [molding and casting project](/digital-fabrication/molding-and-casting/) (in which I made a keycap), as well as the [wild card week project](/digital-fabrication/wildcard-week/)  (in which I made a keyboard case).

The second idea is to build a video feedback setup, which is much more complex and challenging. A significant amount of time was spent on researching about the selection of this topic, both technically and artistically. For this part of information, especially the artistic part, please refer to the [final project references page](/digital-fabrication/final-project/references/).

In short, this project, which can be seen as a minimal viable system of video feedback loop, contains the following parts:

- A camera module, which captures the video signal
- A screen, which displays the video signal
- A camera stand, which holds and presumably move the camera module
- A micro controller, which controls the moment of the camera stand
- A computational device, which processes the video signal

I took a lot of reference while building this project, My contribution to this project contains the following parts:

Electronics design and production:
- Design and manufacture of the programming and driver board for the camera module
- Design and manufacture of the servo driver board for the camera stand

Computer-aided design with laser cutting 3D printing:
- Design and manufacture of the camera stand

Embedded programming:
- Code the ESP-32 CAM module
- Code the servo driver board

Interface and application:
- Touchdesigner postprocessing for the video signal

## BOM (Bill of Materials)

<iframe src="https://docs.google.com/spreadsheets/d/1MTQeWszO3J87ta32rcvPfH8So9RMuDrCcMRPQRgr8dw/edit?usp=sharing" width="100%" height="750" frameborder="0" style="border:0"></iframe>

## Reflection (How will it be evaluated?)

As most of the work is done by now, I will be answering the questions as a reflection of the work I have done.

It is hard to say whether the project is successful or not, as it is not a product that can be evaluated purely by its functionality, but it does fit the criteria of a minimal viable system of video feedback loop. However, the visual effect is not as good as I expected to be honest. This mainly comes from the fact that the camera module has it's limitation in both image quality and transmission speed, which makes it hard to generate clear patterns in the video feedback loop. 

One way of solving this is to use a better camera system, probably a DSLR camera, which can be connected to the computer via USB and identified as a webcam. This will significantly improve the image quality, and also makes it so much easier and more stable to capture the video signal in Touchdesigner. I think the current setup is a good start, as most of the design is easily scalable to a bigger version, the only thing that needs to be changed would probably be the servo motors which are not strong enough to hold a DSLR camera. A slightly bigger servo motor should be able to solve this problem.

All in all, the generation of the video feedback loop, especially the animated patterns, is a somewhat enigmatic process. Numerous elements within the setup can influence the final output, and currently, there is no viable means to simulate, accurately predict, or thoroughly analyze and isolate the factors that contribute to the outcome, at least with the existing technological capabilities we have.

With that being said, I think the core sense of video feedback loop lays in this uncontrollable and unpredictable nature, as how it starts in the analog era. Instead of trying to control the outcome, it would be more accurate to say that video feedback art is an art of uncertainty, and the artist is more like a conductor, who is able to guide the process, but not control it. In this sense, I think the project is successful enough. Nonetheless, it was really fun in the process, and that's what matters the most right?