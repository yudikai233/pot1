---
title: Machine Building
weight: 100
---

# Machine Building

## Intro

In this page, I will document my work in the process of building a Prusa MINI 3D printer together with Group A members.

---

## Information

The printer we built is the [**Original Prusa MINI+ kit**](https://www.prusa3d.com/product/original-prusa-mini-kit-2/), one of the most compact, cost-effective, and user-friendly 3D printers.

{{<img "prusa.webp" "half img_center">}}

## Team members

We decided to differentiate into two groups based on the difference in start time. I participated in group A which started on Friday. Our group members are:
- [Hiski Huovila](https://hiskihuovila.gitlab.io/digital-fabrication/)
- [Ya-Ning Chang](https://ningchang.gitlab.io/digital-fabrication2022/portfolio/)
- [Dikai Yu](https://yudikai233.gitlab.io/test001/)
- [Zhicheng Wang](https://zhicheng_wang.gitlab.io/)

## Building

I took part in most of the building process on Friday. During the session, we successfully constructed the YZ-axis base platform and nearly completed the X-axis with the extruder. Hiski and I primarily focused on the assembly, while Ning assisted by verifying the steps, preparing parts, and taking photos for documenting the process. 

For more comprehensive documentations with an archive of images, you can refer to [Ning's documentation page](https://ningchang.gitlab.io/digital-fabrication2022/portfolio/machine-building/) and also [our group assignment page](https://aaltofablab.gitlab.io/machine-building-2023-group-a/).


{{<img result.jpg "wide shadow img_center" "After first day's assembly. Everything's back to where they should be.">}}

{{<img final.jpg "wide shadow img_center" "Final results">}}

A huge thanks to all my group members who helped in this building session, great teamwork, lots of fun. Also, throughout the process, I gained valuable insights on how to properly assemble a machine, as well as how to utilize various mechanical designs to create such a machine. It was an enjoyable and rewarding experience overall, and nothing beats the feeling of accomplishment when seeing the machine you built performing seamlessly.

{{<img prints.jpg "wide shadow img_center" "And we are off!">}}