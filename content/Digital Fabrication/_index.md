---
title: Digital Fabrication
description: "Description page for the Digital Fabrication course"
bookFlatSection: false
bookCollapseSection: false
weight: 1
---

# **Digital Fabrication**
***

## Links

- [Global course zoom link](http://fabacademy.org/2022/video.html)
- [Fab Academy 2023](https://fabacademy.org/2023/schedule.html)
- [Fab Inventory](https://inventory.fabcloud.io/)
- [MyCourse (DF I)](https://mycourses.aalto.fi/course/view.php?id=35388)
- [MyCourse (DF II)](https://mycourses.aalto.fi/course/view.php?id=35743)
- [MyCourse (DF Studio)](https://mycourses.aalto.fi/course/view.php?id=35742)
- [Attendance](https://mycourses.aalto.fi/mod/attendance/view.php?id=1007213)
- 

## Assignments
- [Week 1 - Project management](/digital-fabrication/project-management)
- [Week 2 - Computer-Aided Design](/digital-fabrication/computer-aided-design)
- [Week 3 - Computer-Controlled Cutting](/digital-fabrication/computer-controlled-cutting)
- [Week 4 - Embedded Programming](/digital-fabrication/embedded-programming)
- [Week 5 - 3D Printing](/digital-fabrication/3d-printing)
- [Week 6 - Electronic Design](/digital-fabrication/electronics-design)
- [Week 7 - Computer-Controlled Machining](/digital-fabrication/computer-controlled-machining)
- [Week 8 - Electronic Production](/digital-fabrication/electronics-production/)
- [Week 9 - Output Device](/digital-fabrication/output-device/)
- [Week 10 - Machine Building](/digital-fabrication/machine-building/)
- [Week 11 - Input Device](/digital-fabrication/input-device/)
- [Week 12 - Molding and Casting](/digital-fabrication/molding-and-casting/)
- [Week 13 - Networking and Communications](/digital-fabrication/networking-and-communications/)
- [Week 14 - Interface and Application](/digital-fabrication/interface-and-application)
- [Week 15 - Wildcard Week](/digital-fabrication/wildcard-week/)
- [Week 16 - Applications and implications](/digital-fabrication/applications-and-implications)
- [Week 17 - Invention, Intellectual Property, and Income](/digital-fabrication/invention-ip-and-income)
- [Week 18 and more - Final Project development](/digital-fabrication/final-project/weekly-progress)


## Archive
- [Digital Fabrication 2023 Archive](https://gitlab.com/aaltofablab/digital-fabrication-2023)
- [Digital Fabrication 2022 Archive](https://gitlab.com/aaltofablab/digital-fabrication-2022)
- [MIT - Lining Yao](http://fab.cba.mit.edu/classes/863.11/people/lining.yao/index.html#Projects)