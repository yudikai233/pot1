---
title: About me
description: "Description page for the author of the website"
weight: 10
---

## About me

{{<img selfie.jpg "img_center wide shadow">}}

Hi, I am Zhicheng Wang, currently studying Human-computer Interaction at Aalto University. 

My passion lies at the intersection of art and technology, particularly in areas such as electronics, interactive installations, motion graphics, and more.

I joined the digital fabrication courses since electronic design and production have always been one of my favorites in the past. However, I do recognize the limitations of traditional prototyping processes, and I am eager to explore and experiment with new approaches to fabrication in the future. I believe there are exciting possibilities waiting to be discovered in the realm of innovative fabrication techniques.