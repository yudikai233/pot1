---
title: Final Project
weight: 20
---

# Shape of time II

## Intro 

{{<img P1010507.JPG "full img_center shadow">}}

***Shape of time II***  was a interaction / installation art project inspired by the game [Shape of time](https://yezi.itch.io/time) made by Zitao Ye.

In this documentation page, I will go through the following sections:
- Thinking process and the idea of the project
- Designing and fabrication process
- Programming
- Results and reflections

## Thinking process & idea

### Thinking process

The core concept of my final project is that I what to make something physical which enables new ways of perceiving things.

My initial idea for the final project is *Sphere-in-a-box*, a rotating platform with a CRT monitor on top of it. It will automatically detect nearby people and turn to them, creating a fake 3D perspective insied the monitor.

{{<img "sketch.jpg" "img_center half shadow">}}

However, as I progress with this idea, I realized there are some problems that are really hard to solve:

- Freeform rotating platform brings cable management problems
- CRT monitors are heavy, so the material used to build the stand should be capable of withstanding a great amount of force, and also the motor that drive the platform should be strong.
- It is difficult to capture and emulate human perspective even if we have the precise data of the human body and eyes.

Thus, I switched to a smaller-scale project, but still keeps the physical rotating concept, which in the end evolved to this.

### Idea

As I mention earlier, this project is hugely inspired by Zitao Ye's game. You can get more information about this game in his talk on the Experimental Gameplay Workshop of GDC 2022.

{{<youtube 7eIfCXU5Rmg>}}

In my work on the topic of time, I aim to build upon the insights introduced by Zitao. His analysis and representations of the survival crisis of modernity highlights the ways in which our perception of time has been affected. The hyper-paced, technologically-driven nature of contemporary life has resulted in time becoming more atomized and compressed. 

Time, which was previously seen as a natural and fluid phenomenon, has now been transformed into a finite and measured resource that must be utilized efficiently. Zitao's argument speaks to the fundamental ways in which the modern world has altered our relationship with the world, leading to a sense of urgency and an ongoing rush to achieve as much as possible within a limited timeframe.

In my discourse on this topic, I aim to place a greater emphasis on the distinction and relationship between the real, continuous time that we perceive through our bodies, and the mechanical, discrete, and atomized time that dominates contemporary life. 

I chose a painting machine that operates within a polar coordinate system as the embodiment of this idea. The pen will initially appear at the edge of the circle and gradually move towards the center over time, causing the radius of the drawing to shrink. Meanwhile, the angle of the drawing is determined by the audience, who can choose the direction of the pen's movement. This line, of course, represents the passage of time, but also serves as a metaphor for the human experience: in the early stages of life, we have a vast range of possibilities and opportunities to explore, but as we near the end of our journey, we all ultimately face the same existential crises.

The contrast between the lines drawn by a real pencil and the discrete point cloud formed by the process of sensor sampling, analog-to-digital conversion, serial transmission to a computer, and further processing, perfectly captures the issues that I wish to address. The pencil lines embody the real, continuous time that is experienced by our bodies, while the digitized point cloud represents the mechanical, atomized time that dominates modern life. Through this machine, I hope to convey the complex relationship between technology, time, and human experience.

## Designing and fabrication

The designing process is fairly simple. As the machine was not very complicated, I directly draw the vector designs and cut them with the laser cutter.

{{<img "laser_cuts.jpg" "img_center half shadow">}}

The machine consists of three primary components: a circular base plate, a central hub that secures the stepper motor and the breadboard, as well as a sliding rail that supports the pen holder. The stepper motor drives the pen holder through a belt transmission mechanism. To simplify the design, I have incorporated a belt fastening device directly onto the pen holder. This ensures that the belt remains securely attached and aligned with the motor shaft, allowing for smooth and consistent operation.

{{<img P1010523.JPG "full img_center shadow">}}

## Programming

### Hardware

For this project, there are two key pieces of equipment: a stepper motor and an gyro/accelerometer sensor. To control these components, I have employed two separate microcontrollers, each dedicated to a specific task. In addition, I have incorporated a joystick into the system, which allows for precise control over the direction and position of the stepper motor. This feature is particularly important for the calibration of the pen holder.

{{<img P1010525.JPG "full img_center shadow" "All the components">}}

{{<img P1010529.JPG "full img_center shadow" "MPU-6050 gyro/accelerometer on top">}}

For now, I am utilizing a serial communication protocol to facilitate communication between the microcontroller and the computer. However, in the future, I plan to implement wireless communication protocols to further enhance the usability of the machine. Code for both the microcontrollers can be found at the bottom of this page.

Here's a video that I showing the control of the stepper motor using the joystick.

{{<video P1010516_x264.mp4 "full">}}

### Software

For the software aspect of the project, I primarily used TouchDesigner as the platform. 

First, I parsed the information received from the serial port and extracted the necessary z-axis rotation data. 

{{<img td1.jpg "full img_center shadow">}}

Next, I used a series of mathematical calculations to determine the coordinates of each point in the point cloud. 

{{<img td2.jpg "full img_center shadow" "Angle data from top, time data from bottom.">}}

Finally, I constructed the point cloud trajectory using a feedback loop, resulting in a visual representation of the pen's path. The discrete effect was created by capping the FPS.

{{<img td3.jpg "full img_center shadow">}}

## Results and reflections

Here is a video where I performed the whole process.

{{<video final_x264.mp4 "full">}}

There are still many areas of this project that can be improved upon. 

- The most obvious one is the use of a wired connection. If I can achieve wireless data transmission, the machine's movement would be even more free and flexible. 
- Additionally, the pencil is too light, resulting in faint lines on the paper. In the future, I will experiment with different types of pencils to draw a clearer contrast between the real-life and simulated trajectories.

Overall, I am quite satisfied with the results that I have achieved so far. Dealing with hardware is always more complicated and difficult than imagined, and I encountered many difficulties during this process. However, these challenges provided valuable opportunities for learning and trying new things. Overall, this has been a meaningful and interesting endeavor, and I plan to continue working on this project in the future.

{{<img P1010531.JPG "full img_center shadow">}}