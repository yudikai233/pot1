---
title: Embodied Interaction
bookFlatSection: false
bookCollapseSection: true
weight: 2
---

# AXM-E7008 Embodied Interaction

---

## Assignments

- [Gesture Assignment](/embodied-interaction/gesture-assignment)
- [Final Project](/embodied-interaction/final-project)

## Reference Links

- [Course website](https://learn.newmedia.dog/courses/embodied-interaction/)
- [Kinect tutorial](https://learn.newmedia.dog/courses/embodied-interaction/tools-and-technology/kinect/)
- [Final project guidelines](https://learn.newmedia.dog/courses/embodied-interaction/final-project/)
- [Showcase (final projects in the past)](https://learn.newmedia.dog/courses/embodied-interaction/showcase/)



